### Hi there, I am [Aabishkar](http://aryalaabishkar.com.np/)👋

Hi, I am Aabishkar Aryal, a self taught programmer from Nepal. 

- 🌱 I’m currently learning Data Science and Machine Learning.
- 💬 Ask me about anything [here](https://github.com/aabishkaryal/aabishkaryal/issues)

##### Languages and tools I like to work with:
  1. Python 
  2. JavaScript (Node, React)
  3. Java (Android)
  4. C++


<!--
**aabishkaryal/aabishkaryal** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
